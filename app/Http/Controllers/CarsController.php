<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\car;

class CarsController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return View
     */
    // public function show($id)
    // {
    //     return view('user.profile', ['user' => User::findOrFail($id)]);
    // }
    public function Index(Request $request){
        $year_filter = $request->year_filter;
        $price_range_filter = $request->price_range_filter;

        if($year_filter && !$price_range_filter){
            $cars = Car::where('date', (int)$year_filter)->orderBy('id', 'DESC')->get();
            return response()->json($cars);
        }
        else if(!$year_filter && $price_range_filter){
            if($price_range_filter=="0-999"){
                $cars = Car::whereBetween('price',[0.00,999.00])->orderBy('id', 'DESC')->get();
            }
            else if($price_range_filter=="1000-1999"){
                $cars = Car::whereBetween('price',[1000.00,1999.00])->orderBy('id', 'DESC')->get();
            }
            else if($price_range_filter=="2000-2999"){
                $cars = Car::whereBetween('price',[2000.00,2999.00])->orderBy('id', 'DESC')->get();
            }
            else{
                $cars = Car::where('price','>',2999.00)->orderBy('id', 'DESC')->get();
            }
            return response()->json($cars);
        }
        else if($year_filter && $price_range_filter){
            if($price_range_filter=="0-999"){
                $cars = Car::where('date', (int)$year_filter)->whereBetween('price',[0.0,999.00])->orderBy('id', 'DESC')->get();
            }
            else if($price_range_filter=="1000-1999"){
                $cars = Car::where('date', (int)$year_filter)->whereBetween('price',[1000.0,1999.00])->orderBy('id', 'DESC')->get();
            }
            else if($price_range_filter=="2000-2999"){
                $cars = Car::where('date', (int)$year_filter)->whereBetween('price',[2000.00,2999.00])->orderBy('id', 'DESC')->get();
            }
            else{
                $cars = Car::where('date', (int)$year_filter)->where('price','>',2999.00)->orderBy('id', 'DESC')->get();
            }
            return response()->json($cars);
        }
        else{
            $cars = Car::orderBy('id', 'DESC')->get();
        }
        return view('cars.index', compact('cars'));
    }
    public function AddModel(Request $request){
        try{
            $model = request()->get('model');
            $price = (float)request()->get('price');
            $date = (int)request()->get('date');
            $make = request()->get('make');

            $car = new Car;
            $car->model = $model;
            $car->price = $price;
            $car->date = $date;
            $car->make = $make;
            $car->save();

            $response = [
                'status' => true
            ];
        }
        catch (\Throwable $th) {
            $response = [
                'status' => false
            ];
        }
        return response()->json($response);
    }
}
