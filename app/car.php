<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class car extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'model' => 'string',
        'price' => 'float',
        'date' => 'int',
        'make' => 'string'
    ];
}
