<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::GET('', 'CarsController@index');
Route::POST('', 'CarsController@index');
Route::POST('/add-model', 'CarsController@AddModel')->name('AddModel');

Auth::routes();

Route::GET('/home', 'HomeController@index')->name('home');
