<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        {{-- <script src="{{ asset('js/app.js') }}" defer></script>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <title>Laravel</title>
    </head>
    <body>
        <div class="container">
            <h1>
                CARS
            </h1>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#AddProductModal">
                    Add car model
            </button>
            <div class="row">
                <div class="col-md-5">
                    <label for="year_filter">SELECT YEAR:</label>
                    <select class="form-control" name='year_filter' id="year_filter">
                        <option value="">-SELECT TO FILTER YEAR-</option>
                        <option value="2001">2001</option>
                        <option value="2002">2002</option>
                        <option value="2003">2003</option>
                        <option value="2004">2004</option>
                        <option value="2005">2005</option>
                        <option value="2006">2006</option>
                    <select>
                </div>
                <div class="col-md-5">
                    <label for="price_range_filter">SELECT PRICE RANGE:</label>
                    <select class="form-control" name="price_range_filter" id="price_range_filter">
                        <option value="">-SELECT TO FILTER PRICE RANGE-</option>
                        <option value="0-999">0-999</option>
                        <option value="1000-1999">1000-1999</option>
                        <option value="2000-2999">2000-2999</option>
                        <option value="2999>">above 2999</option>
                    <select>
                </div>
                <div class="col-md-2">
                    <label for="filter_product_btn"></label>
                    <button type="button" class="btn btn-primary" id="filter_product_btn">FILTER</button>
                </div>
              </div>
            <div id='car-data'>
                @foreach($cars as $car)
                    <div class="row">
                        <div class="column" style="background-color:#aaa;">
                            <h2>{{ $car->model }}</h2>
                            <p>MAKE: {{ $car->make }}</p>
                            <p>PRICE: {{ $car->price }}</p>
                            <p>YEAR: {{ $car->date }}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <!--add car modal here -->
        <div class="modal fade" id="AddProductModal" tabindex="-1" role="dialog" aria-labelledby="AddProductModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="AddProductModalLabel">Add Products</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                        <div class="container">
                                <form id='add_product_form'>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="model">Model Name:</label>
                                            <input type='text' class="form-control"  name="model" id="model" placeholder="model">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="price">Price:</label>
                                            <input type="number" class="form-control" id="price" name="price" value="1" min="1">
                                        </div>
                                    </div>
                                    <div class="row">
                                            <div class="col-md-6">
                                                <label for="make">Make:</label>
                                                <input type="text" id="make" name="make" class="form-control">
                                            </div>
                                        </div>
                                    <div class="row">
                                            <div class="col-md-6">
                                                <label for="date">Date:</label>
                                                <select name="date" id="date" class="form-control">
                                                    <option value="">-SELECT A YEAR-</option>
                                                    <option value="2001">2001</option>
                                                    <option value="2002">2002</option>
                                                    <option value="2003">2003</option>
                                                    <option value="2004">2004</option>
                                                    <option value="2005">2005</option>
                                                    <option value="2006">2006</option>
                                                </select>
                                            </div>
                                    </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="modal_close_btn" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="save_product_btn">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<script>
$(document).ready(function() {
    console.log( "ready!" );
    $('#save_product_btn').click(function(){
                var datastring = $("#add_product_form").serializeArray();
                console.log(datastring);
                var model;
                var price;
                var date;
                var make;

                datastring.forEach(element => {
                    if(element.name=="model"){
                        model = element.value;
                    }
                    if(element.name=="price"){
                        price = element.value;
                    }
                    if(element.name=="date"){
                        date = element.value;
                    }
                    if(element.name=="make"){
                        make = element.value;
                    }
                });
                if(model==null || model== '') {
                    alert('Valid model required');
                }
                else if(price==null || price== '') {
                    alert('Valid price required');
                }
                else if(date==null || date== '') {
                    alert('Valid date required');
                }
                else if(make==null || make== '') {
                    alert('Valid make required');
                }
                else{
                    let data = {
                    "_token": "{{ csrf_token() }}",
                    "model" : model,
                    "date" : parseInt(date),
                    "price" : price,
                    "make" : make
                }
                $.ajax({
                    type: "POST",
                    url: "/add-model",
                    data: data,
                    success: function(response){
                        if(response.status){
                            $("#modal_close_btn").click();
                            window.location.reload();
                        }
                        else{
                            alert('ERROR ESTABLISHING CONNECTION TO THE SERVER');
                        }
                    }
                });
            }
    });
    $('#filter_product_btn').click(function(){
        let yearFilter = $('#year_filter').val();
        let priceRangeFilter = $('#price_range_filter').val();
        let data = {};
        if(yearFilter || priceRangeFilter){
            data ={
                "_token": "{{ csrf_token() }}",
                'year_filter' : yearFilter,
                'price_range_filter' : priceRangeFilter
            }
            $.ajax({
                    type: "POST",
                    url: "/",
                    data: data,
                    success: function(response){
                        $("#car-data").html("");
                        response.forEach(element=>{
                            html='<div class="row">'+
                            '<div class="column" style="background-color:#aaa;">'+
                            '<h2>'+element.model+'</h2>'+
                            '<p>MAKE: '+element.make+'</p>'+
                            '<p>PRICE: '+element.price+'</p>'+
                            '<p>YEAR: '+element.date+'</p>'+
                            '</div>'+
                        '</div>';
                        $("#car-data").append(html);
                        });
                    }
                });
        }
    });

});
</script>
